import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { LPSelectModule } from '../Source/Shared/Select/select.module'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    LPSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

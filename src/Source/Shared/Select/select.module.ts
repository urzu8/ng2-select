import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, ControlValueAccessor } from '@angular/forms';

import {LPSelectComponent} from './Components/select.component';
import {SelectItemComponent} from './Components/selectItem.component';

import {SelectManagerService} from './Services/selectManager.service';
import { InfiniteScrollModule } from "angular2-infinite-scroll/angular2-infinite-scroll";

@NgModule({
    imports: [CommonModule, ReactiveFormsModule, InfiniteScrollModule],
    declarations: [LPSelectComponent, SelectItemComponent],
    exports: [LPSelectComponent]
})

export class LPSelectModule { }
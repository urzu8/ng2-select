import { Directive, forwardRef, Input, OnInit } from '@angular/core';
import { NG_VALIDATORS, FormControl } from '@angular/forms';

function validateSelectFactory(customValidate){

    function validate(data: any, customValidate){
        if(!data) return false;
        if(data === "" || data.length <= 0) return false;

        let res = true;
        if(customValidate) res = customValidate(data);

        return res;
    }

    return (c: FormControl) => {
        console.log('running validation',validate(c.value, customValidate) );
        return validate(c.value, customValidate) ? null : {
            validateSelect:{
                valid: false
            }
        }
    }
}

@Directive({
    selector: "[validateSelect][ngModel], [validateSelect][formControl]",
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => ValidateSelectDirective), multi: true }
    ]
})
export class ValidateSelectDirective{
    validator: Function;

    @Input('customValidate') customValidate: Function;

    ngOnInit(){
        this.validator = validateSelectFactory(this.customValidate);
    }

    validate(c: FormControl){
       return this.validator(c);
    }
    constructor(){}
}
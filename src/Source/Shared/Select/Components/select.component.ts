import {Component, Input, Output, OnInit, EventEmitter, ElementRef, AfterViewInit, ViewChild, forwardRef } from '@angular/core';
import {FormControl, FormGroup, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS} from "@angular/forms";
import {SelectManagerService} from '../Services/selectManager.service';
import { InfiniteScroll } from "angular2-infinite-scroll";
import {Observable} from "rxjs/Observable";

function validateSelectFactory(customValidate){

    function validate(data: any, customValidate){
        if(data){
            if(data === "" || data <= 0) return false;

            let res = true;
            if(customValidate) res = customValidate(data);
        
            return res;
        }
        return false;
    }

    return (c: FormControl) => {
        console.log('running validation',validate(c.value, customValidate) );
        return validate(c.value, customValidate) ? null : {
            validateSelect:{
                valid: false
            }
        }
    }
}

@Component({
    selector: 'lp-select',
    templateUrl: '../Templates/select.html',
    providers: [SelectManagerService,{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => LPSelectComponent),
        multi: true
    },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => LPSelectComponent), multi: true }
    ]
})
export class LPSelectComponent implements OnInit, ControlValueAccessor {

    //input functions
    @Input('load') load;
    @Input('itemFormat') itemFormat: any = false;
    @Input('validateFunc') validateFunc: any = false;

    //event handler
    @Output('onSelected') onSelected = new EventEmitter();

    //settings
    @Input('multiSelect') multiSelect: boolean = false;
    @Input('staticList') staticList: any[] = [];
    @Input('debounceTime') debounceTime : number = 400;
    @Input('placeholder') placeholder: string = 'Search';
    @Input('minCharSearch') minCharSearch: number = 1;
    @Input('disabled') disabled: boolean = false;
    @Input('dropdownHeight') dropdownHeight = 10;
    @Input('resultsSize') resultsSize: number = this.dropdownHeight * 4;
    @Input('preSelect') preSelect: any = false;
    @Input('iconClass') iconClass: string = 'glyphicon glyphicon-list';
    @Input('label') label = false;
    @Input('cacheData') cacheData: boolean = false;//not finished
    @Input('required') required: boolean = false;

    @Input('limitMultiSelectItems') limitMultiSelectItems: number = -1;

    //validation
    @Input('validator') validator: any = false;
    validate(c: FormControl){
        if(this.required){
            return this.validator(c);
        }
        else{
            return null;//this is a passed validation to angular
        }
    }

    //local var
    @ViewChild('dropdown') dropdown;
    keys: Observable<any>;
    userScrolled: boolean = false;
    lpSelect: FormControl;
    propogateChange = (_: any) => {};
    highlightIndex: number = 0;
    showResults = false;
    dropDownState = {
        csiTop: 0,//currently selected index
        csiBottom: 6 
    }
    itemHeight: number = 40;
    dropdownHeightPixel: number = this.itemHeight * this.dropdownHeight;

    //event handlers
    keySwitch = (event) => {
        //console.log('lpSelect', this.lpSelect);
        switch(event.keyCode){
            case 27: //escape
                this.onEscapePressed(event);
                break;
            case 13: //enter
                this.onEnterPressed(event);
                break;
            case 9: //tab
                this.onTabPressed(event);
                break;
            case 38: //up arrow
                this.onUpArrowPressed(event);
                break;
            case 40:
                this.onDownArrowPressed(event);
                break;
            case 8:
                this.onBackspace(event);
                break;
            default:
                this.openOptions();
                break;
        }
    }
    onOverlayClicked(){
        console.log("overlay clicked");
    }
    onBackspace(event: Event){
        this.openOptions();
        if(this.multiSelect && this.lpSelect.value === '') this.manager.popItem();
    }
    onEscapePressed(event: Event){
        //if there is not a current selection, clear input, 
        this.manager.cancelSelection();
        //close options
        this.closeOptions();
        event.preventDefault();
        event.stopPropagation();
    }
    onEnterPressed(event: Event){
        //if there is not a current selection, do nothing
        this.manager.selectHighlight();
        this.closeOptions();
        //set currentSelection by index 
        event.preventDefault();
        event.stopPropagation();
    }
    onTabPressed(event: Event){
        //select highlighted
        this.manager.selectHighlight();
        this.closeOptions();
        //do not prevent default/propogation, that will allow it to bubble to the browser and move to the next control
    }
    onUpArrowPressed(event: Event){
        //if highlight index is > 0 highlight index --
        if(this.highlightIndex > 0) {            
            this.highlightIndex--;
            this.manager.setHighlightSelection(this.highlightIndex);
            console.log('onDownArrowPressed', this.highlightIndex, this.dropDownState);
        }
        this.updateDropdownPosition();
        
        event.preventDefault();
        event.stopPropagation();
    }
    onDownArrowPressed(event: Event){
        //highlight index ++
        //calc if the currently selected element is visible?
        //set scroll to reveal highlighted index element
        this.highlightIndex++;
        this.manager.setHighlightSelection(this.highlightIndex);
        console.log('onDownArrowPressed', this.highlightIndex, this.dropDownState);

        this.updateDropdownPosition();
        this.openOptions();
        
        
        event.preventDefault();
        event.stopPropagation();
    }

    //data control
    clearSearch(){
        if(!this.lpSelect) return;
        if(this.multiSelect && this.lpSelect.value !== ""){
            this.lpSelect.reset();
            this.manager.cancelSelection();
        }
    }
    generateDropDownState(){
        let numItems = Math.floor(this.dropdownHeightPixel / this.itemHeight);
        this.dropDownState.csiTop = this.highlightIndex;
        this.dropDownState.csiBottom = this.highlightIndex + numItems - 1;
    }
    resetDropdown(){
        this.highlightIndex = 0;
        this.generateDropDownState();
    }

    //dom control
    closeOptions(){
        this.showResults = false;       
    }
    openOptions(){
        this.showResults = true;
    }
    scrollToIndex(index){
        setTimeout(()=>{
            if( this.dropdown){ 
                //console.log('scrolling to index', this.itemHeight, index, index - 1, this.dropdown.nativeElement.scrollTop, this.itemHeight * index - 1);
                this.dropdown.nativeElement.scrollTop = (this.itemHeight * (index - 1));
                this.generateDropDownState();
            }
            // if( this.dropdown) console.log('this.dropdown.nativeElement.scrollTop', this.dropdown.nativeElement.scrollTop);
            // if( !this.dropdown) console.log('dropdown not defined');
        }, 0); 
    }
    scrollUpItem(){
        //console.log('scrollUpItem', this.dropdown.nativeElement.scrollTop, this.dropdown.nativeElement.scrollTop - this.itemHeight);
        this.dropdown.nativeElement.scrollTop = this.dropdown.nativeElement.scrollTop - this.itemHeight;
        this.dropDownState.csiTop--;
        this.dropDownState.csiBottom--;  
    }
    scrollDownItem(){
        //console.log('scrollDownItem', this.dropdown.nativeElement.scrollTop, this.itemHeight, this.dropdown.nativeElement.scrollTop + this.itemHeight);
        this.dropdown.nativeElement.scrollTop = this.dropdown.nativeElement.scrollTop + this.itemHeight;
        this.dropDownState.csiTop++;
        this.dropDownState.csiBottom++;
    }
    updateDropdownPosition(){
        
        console.log('updateDropdownPosition: this.showResults', this.showResults, 'dropDownState:', this.userScrolled);
        if(!this.showResults){//then we know that the drop was closed
            //scroll to where the selected index should be
            this.scrollToIndex(this.highlightIndex);
        }
        else if(this.highlightIndex < this.dropDownState.csiTop){
            //move scroll up by one item width
            this.scrollUpItem();
        }
        else if(this.highlightIndex > this.dropDownState.csiBottom){
            //move scroll down by one item
            this.scrollDownItem();
        }
        
    }

    //validation
    registerOnChange(fn){
        this.propogateChange = fn;
    }

    registerOnTouched(){}

    writeValue(value: any){
        if(!value) return;
        if(this.multiSelect){
            this.manager.multiSelectData = value;
        }
        else{
            this.manager.currentSelection = value;
        }
    }

    onSelectionChange(){
        if(this.multiSelect){
            this.propogateChange(this.manager.multiSelectData);
        }
        else{
            this.propogateChange(this.manager.currentSelection);
        }
    }

    ngOnInit(){
        console.log('select');
        if(!this.load && !this.staticList) return console.error('Set a [load] function or provide a [staticList].');
        this.lpSelect = new FormControl('');
        this.lpSelect.valueChanges
            .debounceTime(this.debounceTime)
            .distinctUntilChanged()
            .subscribe((term) => {
                this.resetDropdown();
                this.manager.onSearch(term, this.minCharSearch);
            })
        this.validator = validateSelectFactory(this.validateFunc);
        
        if(this.staticList.length > 0){
            this.manager.data = this.staticList;
            this.manager.infiniteScroll = false;
        }
        else{
            this.manager.setLoad(this.load, this.resultsSize);
            this.manager.initialLoad();
        }
        this.manager.multiSelect = this.multiSelect;
        if(this.preSelect) {
            if(this.multiSelect) this.manager.multiSelectData = this.preSelect;
            else this.manager.currentSelection = this.preSelect;
        }
    }

    ngAfterViewInit(){
        //console.log(this.el.nativeElement ,this.el.nativeElement.getElementsByClassName('list-group'), this.el.nativeElement.getElementsByClassName('.list-group'));
        this.resetDropdown();
    }

    constructor(private el: ElementRef, private manager: SelectManagerService){
        
    }
}
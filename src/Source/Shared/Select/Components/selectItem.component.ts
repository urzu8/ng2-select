import {Component, Input, ViewChild, ElementRef, OnInit} from '@angular/core';

@Component({
    selector: '[lp-select-item]',
    templateUrl: '../Templates/selectItem.html'
})
export class SelectItemComponent {

    @Input('option') option;

    @Input('formatter') formatter;

    @ViewChild('container') container: ElementRef;

    getFormatted(){
        if(this.formatter && this.container) this.container.nativeElement.innerHTML = this.formatter(this.option);

        else return this.option.text;
    }

    ngOnInit(){
        //console.log('item', this);
    }

    constructor(){}
}
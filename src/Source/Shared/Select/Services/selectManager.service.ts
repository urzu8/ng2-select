import {Injectable} from '@angular/core';

import {Observable, ObservableInput} from 'rxjs/Observable'

@Injectable()
export class SelectManagerService {

    //request
    data: any[] = [];
    top: number = 50;
    past: number = 0;
    page: number = 0;
    phrase: string = '';

    //functions
    loadFunc;

    //settings
    infiniteScroll: boolean = true; //if this is false, then we are using a static list
    multiSelect: boolean = false;

    //locals   
    multiSelectData: any[] = []; 
    currentSelection: any = false;
    previousSelection: any = false;
    highlightSelection: any = false;
    isLoading: boolean = false;
    showSpinner: boolean = false;
    touched: boolean = false;
    itemsSearch: string = ''; 

    //data
    popItem(){
        this.multiSelectData.pop();
    }
    removeItem(index){
        this.multiSelectData.splice(index, 1);
    }
    pushSelection(option){
        //check for dupes
        for(let i = 0;i < this.multiSelectData.length; i++)
            if(this.multiSelectData[i].value === option.value)
                return;

        console.log('selectOption multiSelect', option);
        this.multiSelectData.push(option);
        console.log(this.multiSelectData);
    }
    selectOption(option){
        console.log(option, this);
        if(this.multiSelect){
            this.pushSelection(option);
            this.currentSelection = false;
        }
        else{
            this.currentSelection = option;
        }

        this.highlightSelection = option;
    }
    resetSelection(){
        this.multiSelectData = [];
        this.currentSelection = false;
        this.highlightSelection = false;
    }
    loadMore(){
        this.isLoading = true;
        this.startLoadTimer();
        //console.log('load', this.top,' from', this.past, 'searching', this.phrase);
        let params = {};
        this.loadFunc(params).subscribe(this.receiveData)
        // .then(data => {
            
        //     this.data = this.data.concat(data);
        //     //console.log('dataz', this.data);
        // })
    }  
    cancelSelection(){
        if(this.currentSelection) return;
        this.currentSelection = this.previousSelection ? this.previousSelection : false;
    }

    //getters
    getData(){
        return this.multiSelect ? this.multiSelectData : this.currentSelection;
    }
    getCurrentText(){
        return this.currentSelection ? this.currentSelection.text : '';
    }

    //setters
    setHighlightSelection(index){
        this.highlightSelection = this.data[index];
    }
    selectHighlight(){
        this.selectOption(this.highlightSelection);
    }
    selectItem(index){
        this.previousSelection = this.currentSelection;
        this.selectOption(this.data[index]);  
    }
    setLoad(func, pageSize: number){
        this.top = pageSize;
        this.loadFunc = func;
    }
    setPageSize(size){
        this.top = size;
    }

    //event handlers
    onSearch = (phrase: string, minCharSearch: number) => {
        
        if(this.infiniteScroll){//if we are using lazy load data fetching
            if(!phrase && this.touched) {
                this.clearData();
                this.phrase = '';
                this.past = 0;
                this.loadMore();
                return;
            };
            if(phrase.length < minCharSearch) return;
            this.touched = true;
            this.clearData();
            this.phrase = phrase;
            //console.log('on search', this);
            this.loadMore();
        }
        else{
            this.itemsSearch = phrase;
        }
    }
    receiveData = (data) => {
        ////console.log('receiveData', data);
        this.isLoading = false;
        this.showSpinner = false;
        this.data = this.data.length >= 0 ? this.data.concat(data) : data;

        ////console.log('received', this);
    }
    onScroll(){
        //console.log('opn scroll ojsdhfokjsdh');
        if(this.infiniteScroll){
            this.past++;
            this.loadMore();
        }
    }

    //util
    clearData(){
        //console.log('clearing data');
        this.previousSelection = this.currentSelection;
        this.data = [];
    }
    initialLoad(){
        this.loadMore();
    }
    startLoadTimer(){
        if(this.isLoading){
            setTimeout(() => {
                if(this.isLoading) this.showSpinner = true;
            }, 200)
        }
    }
      
    constructor(){}
}
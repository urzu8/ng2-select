## Note

This was developed as a part of a larger internal application. Styling has yet to be rebuilt from ripping out the code. I intend to do it at some point, but for now at least the JS is preserved.

I wrote this from scratch (except infinite scroll) because at the time the available libs for a select2 like solution for angular2 just didn't exist.

## Development server

Run npm install -g @angular/cli

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## features
    @Input('load') //callback for loading the options list
    @Input('itemFormat') //optional function for formatting per item (dates, money, etc)
    @Input('validateFunc') //a validation override for when selected or not isn't enough
    @Output('onSelected') //callback for responding to selections
    @Input('multiSelect') //boolean for multiselect
    @Input('staticList') //if instead of a dynamically loaded list you need a simple client side list
    @Input('debounceTime') //time to wait between keystrokes to fire loadMore
    @Input('placeholder') // sets for input, "Please select an item"
    @Input('minCharSearch') //number of min characters before loadMore is fired with search params
    @Input('disabled') //boolean for dynamically disabling input
    @Input('dropdownHeight') //mostly internal for overloading item list height
    @Input('resultsSize') //internally calculated, but override available. number of visible items at a time
    @Input('preSelect') //pass in an item to preselect
    @Input('iconClass') //in original implementation, styling has icon-addon for input. e.g. "fa-user"
    @Input('label') //sets label
    @Input('cacheData')
    @Input('required')

    @Input('limitMultiSelectItems')

"# README"
import { SelectEnvPage } from './app.po';

describe('select-env App', () => {
  let page: SelectEnvPage;

  beforeEach(() => {
    page = new SelectEnvPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
